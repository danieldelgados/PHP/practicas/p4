<?php

if (isset($_REQUEST['enviar'])) {
    $resultado="";
    
    $numeros = $_REQUEST['numero'];

    $ordenado = $numeros;

    sort($ordenado);

    foreach ($ordenado as $value) {
       $resultado.="$value ";
    }

    if ($ordenado == $numeros) {
        $resultado.="<br>Los datos estaban ordenados";
    } else {
        $resultado.='<br>Los datos no estaban ordenados';
    }
    $datos=[
        "titulo"=>"Los numeros ordenados ascendentemente",
        "content"=>"result.php",
        "mensaje"=>"si quieres probar de nuevo",
        "pie"=>"Daniel Delgado",
        "resultado"=>$resultado,
    ];
}else{
    $datos=[
        "titulo"=>"Introduce los datos",
        "content"=>"form.php",
        "pie"=>"Daniel Delgado"
    ];
}

