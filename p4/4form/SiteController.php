<?php

function actionMedia() {
    $num = $_REQUEST['num'];
    $sum = 0;
    $media = 0;
    $c = 0;
    foreach ($num as $value) {
        $c++;
        $sum += $value;
        $media = $sum / $c;
    }

//    echo "$media";
    
    return[
        "titulo"=>"Media",
        "resultado"=>"$media",
        "content"=>"media.php",
    ];
}

function actionModa() {
    $num = $_REQUEST['num'];

    $nRepeticiones = array_count_values($num);
    arsort($nRepeticiones);
    $valor = array_keys($nRepeticiones);
    $moda = array_shift($valor);

//    echo "$moda";
    
    return[
        "titulo"=>"Moda",
        "resultado"=>"$moda",
        "content"=>"moda.php",
    ];
}

function actionMediana() {
    $num = $_REQUEST['num'];
    $c = count($num);
    $medio = floor($c / 2);
    $mediana1 = $num[$medio];


    if ($c % 2 == 0) {
        $sum = ($num[$medio]) + ($num[$medio - 1]);

        $mediana = $sum / 2;
//        echo "La mediana es: $prom ";
    } else {
       $mediana=$mediana1;
    }
    
    return[
        "titulo"=>"Mediana",
        "resultado"=>"$mediana",        
        "content"=>"mediana.php",
       
    ];
}

function actionDesviacion() {
    $num = $_REQUEST['num'];
    $sum = 0;
    $c = 0;
    $media = 0;
    $resultado = [];
    $eleva = [];
    $r2 = 0;

//sumamos todos los elementos del array y calculamos la media
    foreach ($num as $value) {
        $c++;
        $sum += $value;
        $media = $sum / $c;
    }
    echo "$media";

// vamos a restar cada elemento por la media y luego elvamos cada numero por 2
    foreach ($num as $value) {

        $resultado[] += $value - $media;
    }
//    var_dump($resultado);
//elevamos cada ressultado previo *2

    foreach ($resultado as $value1) {
        $eleva[] += pow($value1, 2);
        $r2 = array_sum($eleva);
    }
//    var_dump($r2);
// dividimos el resultado previo entre la media
    $r3 = $r2 / $media;
//    echo $r3;
//finalmente tenemos que calcular la raiz cuadrada del resultado anterior
    $desv = sqrt($r3);
//    echo "<br>$desv";
    
    return[
        "titulo"=>"Desviacion",
        "resultado"=>"$desv",
        "content"=>"desviacion.php",
    ];
}

function actionLoad(){
    
    
    return[
        "titulo"=>"Elige que deseas calcular",        
        "content"=>"formulario.php",
    ];
}


if(empty($_REQUEST)){
   
    $datos= actionLoad();
}elseif(!empty($_REQUEST["media"])){
    
    $datos= actionMedia();
    
    
}elseif(!empty($_REQUEST["moda"])){
    
    $datos= actionModa();
}elseif(!empty($_REQUEST["mediana"])){
    
    $datos= actionMediana();
}else{
    
    $datos= actionDesviacion();    
}
