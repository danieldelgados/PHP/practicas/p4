<?php


function actionMedia() {
    $num = $_REQUEST['num'];
    //compruebo si es array es numero o string para saber de donde viene
    
    if(gettype($num)=="string"){
        $num= explode(',', $num);
    }
    $sum = 0;    
    $c =count($num);
    foreach ($num as $value) {       
        $sum += $value;               
    }   
    $media = $sum / $c;
    /*
     * antes de enviarlo a la vista los numeros convierto a string     
     */
    $num=implode(",",$num);
    return[
        "titulo"=>'Resultado de Media',
        "resultado"=>$media,
        "vista"=>"resultado.php",
        "mensaje"=>"La media calculada es",
        "numeros"=>$num,
    ];
}

function actionModa() {
    $num = $_REQUEST['num'];
    if(gettype($num)=="string"){
        $num= explode(',', $num);
    }
    $nRepeticiones = array_count_values($num);
    arsort($nRepeticiones);
    $valor = array_keys($nRepeticiones);
    $moda = array_shift($valor);


    $num=implode(",",$num);
    return[
        "titulo"=>"Resultado de Moda",
        "resultado"=>"$moda",
        "vista"=>"resultado.php",
        "mensaje"=>"La moda calculada es",
        "numeros"=>$num,
        
    ];
}

function actionMediana() {
    $num = $_REQUEST['num'];
     if(gettype($num)=="string"){
        $num= explode(',', $num);
    }
    $c = count($num);
    $medio = floor($c / 2);
    $mediana1 = $num[$medio];


    if ($c % 2 == 0) {
        $sum = ($num[$medio]) + ($num[$medio - 1]);

        $mediana = $sum / 2;

    } else {
       $mediana=$mediana1;
    }
    $num=implode(",",$num);
    return[
        "titulo"=>"Resultado de Mediana",
        "resultado"=>"$mediana",        
        "vista"=>"resultado.php",
        "mensaje"=>"La mediana calculada es",
        "numeros"=>$num,
        
       
    ];
}

function actionDesviacion() {
    $num = $_REQUEST['num'];
    if(gettype($num)=="string"){
        $num= explode(',', $num);
    }
    $sum = 0;
    $c = 0;
    $media = 0;
    $resultado = [];
    $eleva = [];
    $r2 = 0;

//sumamos todos los elementos del array y calculamos la media
    foreach ($num as $value) {
        $c++;
        $sum += $value;
        $media = $sum / $c;
    }


// vamos a restar cada elemento por la media y luego elvamos cada numero por 2
    foreach ($num as $value) {

        $resultado[] += $value - $media;
    }

//elevamos cada ressultado previo *2

    foreach ($resultado as $value1) {
        $eleva[] += pow($value1, 2);
        $r2 = array_sum($eleva);
    }
    $n=count($num);
// dividimos el resultado previo entre la media
    $r3 = $r2 / $n;
//    echo $r3;
//finalmente tenemos que calcular la raiz cuadrada del resultado anterior
    $desv = sqrt($r3);
//    echo "<br>$desv";
    $num=implode(",",$num);
    return[
        "titulo"=>"Resultado de Desviacion",
        "resultado"=>"$desv",
        "vista"=>"resultado.php",
        "mensaje"=>"La desviación típica calculada es",
        "numeros"=>$num,
    ];
}

function actionFormulario(){
    
    
    return[
        "titulo"=>"Elige que deseas calcular",        
        "vista"=>"formulario.php",
    ];
}

//aqui asigno cada accion a una variable del mismo nombre y luego puedo acceder al array que devuleve cada accion. 
if(isset($_REQUEST['boton'])){
   switch($_REQUEST['boton']){
       case 'media':
           $datos=actionMedia();
           break;
       case 'moda':
           $datos= actionModa();
           break;
       case 'mediana':
           $datos= actionMediana();
           break;
       case 'desviacion':
           $datos= actionDesviacion();
           break;
       default :
           $datos= actionFormulario();
   }
    
}else{
    $datos= actionFormulario();
}
