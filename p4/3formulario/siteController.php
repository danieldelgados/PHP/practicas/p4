<?php

function actionResultado() {
    $frase = $_REQUEST['frase'];

    $contador = [];
    $contador["a"] = substr_count(strtolower($frase), "a");
    $contador["e"] = substr_count(strtolower($frase), "e");
    $contador["i"] = substr_count(strtolower($frase), "i");
    $contador["o"] = substr_count(strtolower($frase), "o");
    $contador["u"] = substr_count(strtolower($frase), "u");
    
    return [
        "titulo"=>"Calcular el numero de vocales",
        "content"=>"vocales.php",
        "curso"=>"Año 2018",
        "pie"=>"Resultados de la aplicacion",
        "nVocales"=>$contador,
    ];
}

function actionIndex() {
    return [
        "titulo" => "Calcular el numero de vocales",
        "content" => "form.php",
        "curso" => "Año 2018",
        "pie" => "Página principal de la aplicación",
        "mensaje" => "Introduce una frase",
    ];
}

if (isset($_REQUEST['enviar'])) {
    // aqui podemos llamar a una accion carga si han escrito algo
    $datos=actionResultado();
} else {
    //aqui llamamos a otra acción carga el formulario
    $datos = actionIndex();
}

